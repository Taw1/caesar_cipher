# Caesar cipher

> Единственное, чего люди не забывают, это нечто нераскрытое. Ничто не живет так долго, как
неразгаданная тайна.” — (C) Джон Фаулз.

Caesar cipher project is on-line encrypt-like service. The system allow users to encrypt/decrypt sentences, shows the frequency chart and also trying to guess right key.


### Tech

Caesar cipher uses a number of open source projects to work properly:

* [Flask] - Python web framework
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [Google chart] - JS charts
* [jQuery] - duh


### Installation

Use following commands in your shell.

* Install virtual environment and activate it:

    ```sh
    $ python3 -m venv .venv
    $ source .venv/bin/activate
    ```

* Install packages:

    ```sh
    $ pip install -r requirements.txt
    ```

* Run caesar cipher app by typing following command:

     ```sh
    $ python manage.py runserver
    ```

