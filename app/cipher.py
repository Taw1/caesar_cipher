LANGUAGES = {'en': ['a', 'b', 'c', 'd', 'e',       # Dict with language code and language letter
                    'f', 'g', 'h', 'i', 'j',
                    'k', 'l', 'm', 'n', 'o',
                    'p', 'q', 'r', 's', 't',
                    'u', 'v', 'w', 'x', 'y', 'z']
             }

FREQUENCY = {'en': {'a': 8.17, 'b': 1.49, 'c': 2.78, 'd': 4.25, 'e': 12.7,    # Dict with language code and letter
                    'f': 2.23, 'g': 2.02, 'h': 6.09, 'i': 6.97, 'j': 0.15,    # frequency
                    'k': 0.77, 'l': 4.03, 'm': 2.41, 'n': 6.75, 'o': 7.51,
                    'p': 1.93, 'q': 0.1, 'r': 5.99, 's': 6.33, 't': 9.06,
                    'u': 2.76, 'v': 0.98, 'w': 2.36, 'x': 0.15, 'y': 1.97, 'z': 0.05}
             }


def get_letter_wheel(language: str) -> list:
    """Trying to get letter for language
        Args:
            language: Lang code.

        Returns:
            The return value. List with letter for success, raise exception otherwise.
    """
    letters_wheel = LANGUAGES.get(language)
    if not letters_wheel:
        raise Exception('Language {} not supported!'.format(language))
    return letters_wheel


def c_encode(string: str, language='en', shift=0) -> str:
    """Encrypt string with defined shift
        Args:
            string: String to encrypt.
            language: Lang code.
            shift: Cipher key.

        Returns:
            The return value. Encrypted string.
    """
    letters_wheel = get_letter_wheel(language)
    return ''.join((letters_wheel[(letters_wheel.index(letter.lower())+shift) % len(letters_wheel)].upper()
                    if letter.istitle()
                    else letters_wheel[(letters_wheel.index(letter.lower())+shift) % len(letters_wheel)])
                   if letter.isalpha() else letter
                   for letter in string)


def c_decode(string: str, language='en', shift=0) -> str:
    """Decrypt string with defined shift
        Args:
                string: String to decrypt.
                language: Lang code.
                shift: Cipher key.

            Returns:
                The return value. Decrypted string.
    """
    letters_wheel = get_letter_wheel(language)
    return ''.join((letters_wheel[(letters_wheel.index(letter.lower()) - (shift % len(letters_wheel)))].upper()
                    if letter.istitle()
                    else letters_wheel[(letters_wheel.index(letter.lower()) - (shift % len(letters_wheel)))])
                   if letter.isalpha() else letter
                   for letter in string)


def cipher_crack(string: str, language='en') -> int:
    """Trying to crack cipher with help of math statistic
        Args:
            string: String to crack.
            language: Lang code.

        Returns:
            The return value. Possible key.
    """

    def letters_count(string: str) -> dict:
        """Counting letters in string
            Args:
                string: String to decompose.

            Returns:
                The return value. Dict {letter: count}.
        """
        return {letter: string.count(letter) for letter in string.lower() if letter.isalpha()}

    frequency = FREQUENCY[language]
    frequencys_list = []
    for i in range(0, len(frequency.keys())):
        frequencys_list.append(
            sum(
                freq * frequency[letter] for letter, freq in letters_count(c_decode(string=string, shift=i)).items()
            )
        )
    return frequencys_list.index(max(frequencys_list))
