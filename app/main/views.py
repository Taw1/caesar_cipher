from . import main

from flask import render_template, request, jsonify
from ..cipher import c_decode, c_encode, cipher_crack


@main.route('/')
def index():
    return render_template('index.html')


@main.route('/encode', methods=['POST'])
def encode():
    req_data = request.json
    return jsonify({'encoded_string': c_encode(string=req_data['string'], shift=int(req_data['shift']))})


@main.route('/decode', methods=['POST'])
def decode():
    req_data = request.json
    return jsonify({'decoded_string': c_decode(string=req_data['string'], shift=int(req_data['shift']))})


@main.route('/crack', methods=['POST'])
def crack():
    req_data = request.json
    guessed_shift = cipher_crack(req_data['string'])
    print(guessed_shift)
    return jsonify({'guessed_string': c_decode(req_data['string'], shift=guessed_shift)}) if guessed_shift != 0\
        else jsonify({})
