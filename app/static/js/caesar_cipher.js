//setup before functions
google.charts.load('current', {'packages':['bar']});
google.charts.setOnLoadCallback(drawStuff);

var chart;                      //chart variable
var typingTimer;                //timer identifier
var doneTypingInterval = 2000;  //time in ms, 2 second for example

//on keyup, start the countdown
$('#in-box').on('keyup', function () {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

//on keydown, clear the countdown
$('#in-box').on('keydown', function () {
    clearTimeout(typingTimer);
});

//user is "finished typing," do something
function doneTyping () {
    //do something
    chart.draw(new google.visualization.arrayToDataTable(letters_let_count($("#in-box").val())))
    req_data = {string: $("#in-box").val()};
    $.ajax({
        url: window.location.href+'crack',
        data: JSON.stringify(req_data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        type: 'POST',
        success: function(data) {
            if(data['guessed_string']){
                flashMessage(data['guessed_string']);
            }
        }
    });
    return false;
}

$('#btnEncode').bind('click', function() {
    req_data = {string: $("#in-box").val(),
                shift: $("#shift").val()
            };
    $.ajax({
        url: window.location.href+'encode',
        data: JSON.stringify(req_data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        type: 'POST',
        success: function(data) {
            $("#out-box").val(data['encoded_string'])
        }
    });
  return false;
});

$('#btnDecode').bind('click', function() {
    req_data = {string: $("#in-box").val(),
                shift: $("#shift").val()
            };
    $.ajax({
        url: window.location.href+'decode',
        data: JSON.stringify(req_data, null, '\t'),
        contentType: 'application/json;charset=UTF-8',
        type: 'POST',
        success: function(data) {
            $("#out-box").val(data['decoded_string'])
        }
    });
  return false;
});

function drawStuff(let_count) {

    var data = new google.visualization.arrayToDataTable(
        [
            ['Letter', 'Percentage'],
            ['a', 0],
            ['b', 0],
            ['c', 0],
            ['d', 0],
            ['e', 0],
            ['f', 0],
            ['g', 0],
            ['h', 0],
            ['i', 0],
            ['j', 0],
            ['k', 0],
            ['l', 0],
            ['m', 0],
            ['n', 0],
            ['o', 0],
            ['p', 0],
            ['q', 0],
            ['r', 0],
            ['s', 0],
            ['t', 0],
            ['u', 0],
            ['v', 0],
            ['w', 0],
            ['x', 0],
            ['y', 0],
            ['z', 0]
        ]
    );

    var options = {
      width: $("#diagram").width(),
      animation:{
        duration: 1000,
        easing: 'out',
      },
      legend: { position: 'none' },
      axes: {
        x: {
          0: { side: 'bot', label: ''} // Top x-axis.
        }
      },
      bar: { groupWidth: "90%" }
    };

    chart = new google.charts.Bar(document.getElementById('chartContainer'));
    console.log(chart)
    // Convert the Classic options to Material options.
    chart.draw(data, google.charts.Bar.convertOptions(options));
};

function UpdateStaff(data){
    chart.draw(data)
}


function flashMessage(message) {
    var div = document.createElement("div");
    div.className = 'alert alert-warning';
    var let_html = '<button type="button" class="close" data-dismiss="alert">&times;</button>:message';
    let_html = let_html.replace(':message', message);
    div.innerHTML = let_html;
    var msg_cont = document.getElementsByClassName('container flash')[0]
    $(msg_cont).prepend(div)
}

function letters_let_count(string){
    var counted_letters = []
    counted_letters.push(['Letter', 'Percentage'])
    var alphabet = ['a', 'b', 'c', 'd', 'e',
                    'f', 'g', 'h', 'i', 'j',
                    'k', 'l', 'm', 'n', 'o',
                    'p', 'q', 'r', 's', 't',
                    'u', 'v', 'w', 'x', 'y', 'z']
    string = string.replace(/[^A-Z]/gi, "").toLowerCase()
    var str_len = string.length 
    for (var i = 0; i < alphabet.length; i++) {
        var re = new RegExp(alphabet[i],"gi");
        re_str = string.match(re)
        if(re_str == null){
            len = 0
        }
        else{
            len = (re_str.length/str_len)*100;
        }
        counted_letters.push([alphabet[i], len])
    }
    return counted_letters
}
