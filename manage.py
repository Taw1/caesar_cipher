#!/usr/bin/env python

from app import create_app
from app.cipher import c_encode, cipher_crack, c_decode
from flask_script import Manager, Shell

app = create_app('default')
manager = Manager(app)


def make_shell_context():
    return dict(app=app, c_encode=c_encode, c_decode=c_decode, cipher_crack=cipher_crack)

manager.add_command("shell", Shell(make_context=make_shell_context))


if __name__ == '__main__':
    manager.run()
