import unittest
from app.cipher import c_encode, c_decode, get_letter_wheel, cipher_crack


class CipherTestCase(unittest.TestCase):
    def test_get_letter_wheel_exception_if_language_not_exists(self):
        language = 'ru'
        with self.assertRaisesRegex(Exception, 'Language {} not supported!'.format(language)):
            get_letter_wheel(language)

    def test_get_letter_wheel_returns_list_if_language_exists(self):
        language = 'en'
        expected = ['a', 'b', 'c', 'd', 'e',
                    'f', 'g', 'h', 'i', 'j',
                    'k', 'l', 'm', 'n', 'o',
                    'p', 'q', 'r', 's', 't',
                    'u', 'v', 'w', 'x', 'y',
                    'z']
        actual = get_letter_wheel(language)
        self.assertEqual(expected, actual)

    def test_cipher_encode(self):
        string = 'Hello world!'

        shift = 0
        expected = 'Hello world!'
        actual = c_encode(string=string, shift=shift)
        self.assertEqual(expected, actual)  # ROT0
        shift = 17
        expected = 'Yvccf nficu!'
        actual = c_encode(string=string, shift=shift)
        self.assertEqual(expected, actual)  # ROT17
        shift = 25
        expected = 'Gdkkn vnqkc!'
        actual = c_encode(string=string, shift=shift)
        self.assertEqual(expected, actual)  # ROT25
        shift = 36
        expected = 'Rovvy gybvn!'
        actual = c_encode(string=string, shift=shift)
        self.assertEqual(expected, actual)  # ROT36

    def test_cipher_decode(self):
        expected = 'Hello world!'

        string = 'Hello world!'
        shift = 0
        actual = c_decode(string=string, shift=shift)
        self.assertEqual(expected, actual)  # ROT0
        shift = 17
        string = 'Yvccf nficu!'
        actual = c_decode(string=string, shift=shift)
        self.assertEqual(expected, actual)  # ROT17
        shift = 25
        string = 'Gdkkn vnqkc!'
        actual = c_decode(string=string, shift=shift)
        self.assertEqual(expected, actual)  # ROT25
        shift = 36
        string = 'Rovvy gybvn!'
        actual = c_decode(string=string, shift=shift)
        self.assertEqual(expected, actual)  # ROT36

    def test_cipher_crack_returns_correct_shift(self):
        shift = 7
        string = c_encode("""Van Rossum was born and raised in the Netherlands, where he received a master's degree in
                             mathematics and computer science from the University of Amsterdam in 1982. He later worked
                             for various research institutes, including the Dutch Centrum Wiskunde & Informatica (CWI),
                             Amsterdam, the United States National Institute of Standards and Technology (NIST),
                             Gaithersburg, Maryland, and the Corporation for National Research Initiatives (CNRI),
                             Reston, Virginia.""",
                          shift=shift)
        expected = 7
        actual = cipher_crack(string)
        self.assertEqual(actual, expected)

        shift = 0
        string = c_encode("""Van Rossum was born and raised in the Netherlands, where he received a master's degree in
                             mathematics and computer science from the University of Amsterdam in 1982. He later worked
                             for various research institutes, including the Dutch Centrum Wiskunde & Informatica (CWI),
                             Amsterdam, the United States National Institute of Standards and Technology (NIST),
                             Gaithersburg, Maryland, and the Corporation for National Research Initiatives (CNRI),
                             Reston, Virginia.""",
                          shift=shift)
        expected = 0
        actual = cipher_crack(string)
        self.assertEqual(actual, expected)
